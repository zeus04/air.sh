#!/bin/bash
## This is the MASTER BRANCH 

echo 'SCRIPT INITIATED'

## 1.2
## interface reset and macchange


# wlan1
echo "Wlan1"
echo -n "stop wlan1mon if up....";airmon-ng stop wlan1mon  &> /dev/null; echo " done.";
echo -n "bringing down wlan1 if up....";ifconfig wlan1 down  &> /dev/null; echo " done.";
echo -n "changing mac to random....";macchanger -r wlan1  &> /dev/null; echo " done.";
echo -n "bringing up wlan1....";ifconfig wlan1 up&> /dev/null; echo " done.";
echo -n "putting wlan1 in monitor mode....";airmon-ng start wlan1  &> /dev/null; echo " done.";
# wlan1
echo "Wlan2"
echo -n "stop wlan2mon if up....";airmon-ng stop wlan2mon  &> /dev/null; echo " done.";
echo -n "bringing down wlan2 if up....";ifconfig wlan2 down  &> /dev/null; echo " done.";
echo -n "changing mac to random....";macchanger -r wlan2  &> /dev/null; echo " done.";
echo -n "bringing up wlan2....";ifconfig wlan2 up&> /dev/null; echo " done.";
echo -n "putting wlan2 in monitor mode....";airmon-ng start wlan2  &> /dev/null; echo " done.";
# wlan1
echo "Wlan3"
echo -n "stop wlan3mon if up....";airmon-ng stop wlan3mon  &> /dev/null; echo " done.";
echo -n "bringing down wlan3 if up....";ifconfig wlan3 down  &> /dev/null; echo " done.";
echo -n "changing mac to random....";macchanger -r wlan3  &> /dev/null; echo " done.";
echo -n "bringing up wlan3....";ifconfig wlan3 up&> /dev/null; echo " done.";
echo -n "putting wlan3 in monitor mode....";airmon-ng start wlan3  &> /dev/null; echo " done.";
# wlan1
echo "Wlan4"
echo -n "stop wlan4mon if up....";airmon-ng stop wlan4mon  &> /dev/null; echo " done.";
echo -n "bringing down wlan4 if up....";ifconfig wlan4 down  &> /dev/null; echo " done.";
echo -n "changing mac to random....";macchanger -r wlan4  &> /dev/null; echo " done.";
echo -n "bringing up wlan4....";ifconfig wlan4 up&> /dev/null; echo " done.";
echo -n "putting wlan4 in monitor mode....";airmon-ng start wlan4  &> /dev/null; echo " done.";

echo "mac-adresses randomized"
wait
echo '4 NICS NOW IN MONITOR MODE'

# 3. tmux/airodump
#tmux new-window -t $SESSION:1 -n 'wlan1mon'
#tmux send-keys "airodump-ng  -w wlan1mon_ch1_dump -c 1 wlan1mon" C-m  

#xterm -e airodump-ng -w wlan1mon_ch_all__dump  wlan1mon &
#xterm -e airodump-ng -w wlan2mon_ch_all__dump  wlan2mon &
#xterm -e airodump-ng -w wlan3mon_ch_all__dump  wlan3mon &
#xterm -e airodump-ng -w wlan4mon_ch_all__dump  wlan4mon &

echo trying 4-card capture
airodump-ng -w /root/pcap/wlan1234mon_ch_all__dump -c 1,6,11  wlan1mon,wlan2mon,wlan3mon,wlan4mon || 
airodump-ng -w /root/pcap/wlan1234mon_ch_all__dump  -c 1,6,11 wlan1mon,wlan2mon,wlan3mon 
